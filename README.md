**Trabajo practico sd banco**

Por una mala lectura actualmente el sistema esta implementado sobre un servidor UDP, lo cual en las especificaciones
pedia que sea implementado sobre un servidor TCP, el sistema tiene implementado una serie de funcionalidades, no todas
las que se piden y las implementadas no respeta parametros expuestos en los requerimientos.

*Una recomendacion para empezar a modificar el sistema, crear una rama que estire de developer exponiendo en el nombre
de la rama con un verbo infinitvo que funcion se implementa en la rama.

*El Sistema respeta la estructura de archivos de graddle, el manejo de de dependencias se realiza tambien bajo graddle

*Las librerias utilizadas utilizadas

1. Postgresql42.2.2 (conexion a la base de datos)
2. Hibernate5.2.16.Final (ORM)
3. Gson2.8.2 (manejo de datos en formato json)

---

## Estructura de proyecto

El sistema respeta la estructura de archivos graddle
``````
banco_sd
└── src
    ├── main
    │   ├── java
    │   │   └── com
    │   │       └── servidor
    │   │           ├── app
    │   │           ├── entity
    │   │           └── statics
    │   └── resources
    └── test
        ├── java
        │   └── com
        │       └── servidor
        └── resources
``````

