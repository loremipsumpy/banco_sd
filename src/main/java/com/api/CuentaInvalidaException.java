package com.api;

public class CuentaInvalidaException extends Exception {
    public CuentaInvalidaException() {
        this("El numero de cuenta ingresado no coincide a la de ningun usuario");
    }

    private CuentaInvalidaException(String message) {
        super(message);
    }
}
