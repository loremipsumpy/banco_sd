package com.api;

import com.api.statics.Error;
import com.api.statics.Request;
import com.google.gson.Gson;
import com.servidor.entity.Cuenta;
import com.servidor.entity.CuentaMovimiento;
import com.servidor.entity.Movimiento;
import com.servidor.statics.TipoMovimiento;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class Comunication {
    private Request request;
    private Integer usuarioCi;
    private Long cuentaId1;
    private Long cuentaId2;
    private Long movimientoId;
    private Double monto;
    private Error error;

    public Comunication(Request request, Integer usuarioCi, Long cuentaId1, Long cuentaId2, Long movimientoId, Double monto, Error error) {
        this.request = request;
        this.usuarioCi = usuarioCi;
        this.cuentaId1 = cuentaId1;
        this.cuentaId2 = cuentaId2;
        this.movimientoId = movimientoId;
        this.monto = monto;
        this.error = error;
    }

    //constructor Transferir
    public Comunication(Request request, Long cuentaId1, Long cuentaId2, Double monto) {
        this.request = request;
        this.usuarioCi = null;
        this.cuentaId1 = cuentaId1;
        this.cuentaId2 = cuentaId2;
        this.movimientoId = null;
        this.monto = monto;
        this.error = Error.SinError;
    }

    //constructor Consultar Saldo
    public Comunication(Request request, Long cuentaId1) {
        this.request = request;
        this.usuarioCi = null;
        this.cuentaId1 = cuentaId1;
        this.cuentaId2 = null;
        this.movimientoId = null;
        this.monto = null;
        this.error = Error.SinError;
    }

    //constructor Depositar, Extraer
    public Comunication(Request request, Long cuentaId1, Double monto) {
        this.request = request;
        this.usuarioCi = null;
        this.cuentaId1 = cuentaId1;
        this.cuentaId2 = null;
        this.movimientoId = null;
        this.monto = monto;
        this.error = Error.SinError;
    }

    //constructor simple error
    public Comunication(Error error) {
        this.request = null;
        this.usuarioCi = null;
        this.cuentaId1 = null;
        this.cuentaId2 = null;
        this.movimientoId = null;
        this.monto = null;
        this.error = error;
    }

    public String response() throws CuentaSinSaldoException, CuentaInvalidaException, MismaCuentaException {
        SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        Cuenta cuenta;
        Movimiento movimiento;
        CuentaMovimiento cuentaMovimiento;
        switch (request) {
            case consultarSaldo:
                cuenta = session.get(Cuenta.class, cuentaId1);
                session.close();
                if (cuenta == null)
                    throw new CuentaInvalidaException();
                return (cuenta.toJson());
            case depositar:
                cuenta = session.get(Cuenta.class, cuentaId1);
                if (cuenta == null) {
                    session.close();
                    throw new CuentaInvalidaException();
                }
                movimiento = new Movimiento(monto);
                cuentaMovimiento = new CuentaMovimiento(cuenta, movimiento, TipoMovimiento.Ingreso);
                cuenta.setSaldo(cuenta.getSaldo() + monto);
                session.update(cuenta);
                session.save(cuentaMovimiento);
                session.save(movimiento);
                session.getTransaction().commit();
                session.close();
                return (cuentaMovimiento.toJson());
            case extraer:
                cuenta = session.get(Cuenta.class, cuentaId1);
                if (cuenta == null) {
                    session.close();
                    throw new CuentaInvalidaException();
                }
                movimiento = new Movimiento(monto);
                cuentaMovimiento = new CuentaMovimiento(cuenta, movimiento, TipoMovimiento.Egreso);
                if (cuenta.getSaldo() - monto <= 0) {
                    session.close();
                    throw new CuentaSinSaldoException();
                }
                cuenta.setSaldo(cuenta.getSaldo() - monto);
                session.update(cuenta);
                session.save(cuentaMovimiento);
                session.save(movimiento);
                session.getTransaction().commit();
                session.close();
                return (cuentaMovimiento.toJson());
            case Transferir:
                if (cuentaId1.equals(cuentaId2)) {
                    session.close();
                    throw new MismaCuentaException();
                }
                Cuenta cuentaDeudor = session.get(Cuenta.class, cuentaId1);
                Cuenta cuentaAcrededor = session.get(Cuenta.class, cuentaId2);
                if (cuentaDeudor == null || cuentaAcrededor == null) {
                    session.close();
                    throw new CuentaInvalidaException();
                }
                if (cuentaDeudor.getSaldo() - monto <= 0) {
                    session.close();
                    throw new CuentaSinSaldoException();
                }
                cuentaDeudor.setSaldo(cuentaDeudor.getSaldo() - monto);
                cuentaAcrededor.setSaldo(cuentaAcrededor.getSaldo() + monto);
                movimiento = new Movimiento(monto);
                CuentaMovimiento movimientoDeudor = new CuentaMovimiento(cuentaDeudor, movimiento, TipoMovimiento.Egreso);
                CuentaMovimiento movimientoAcrededor = new CuentaMovimiento(cuentaAcrededor, movimiento, TipoMovimiento.Ingreso);
                session.update(cuentaDeudor);
                session.update(cuentaAcrededor);
                session.save(movimiento);
                session.save(movimientoDeudor);
                session.save(movimientoAcrededor);
                session.getTransaction().commit();
                session.close();
                return (movimientoDeudor.toJson());
            default:
                return "Error de Request";
        }
    }

    public String toJson() {
        Gson gson = new Gson();
        return (gson.toJson(this));
    }
}
