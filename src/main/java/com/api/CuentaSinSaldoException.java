package com.api;

public class CuentaSinSaldoException extends Exception {
    public CuentaSinSaldoException() {
        this("La cuenta no posee suficiente saldo para realizar la transaccion");
    }

    private CuentaSinSaldoException(String message) {
        super(message);
    }
}
