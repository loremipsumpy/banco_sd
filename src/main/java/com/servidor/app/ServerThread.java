package com.servidor.app;

import com.api.Comunication;
import com.api.CuentaInvalidaException;
import com.api.CuentaSinSaldoException;
import com.api.MismaCuentaException;
import com.api.statics.Error;
import com.google.gson.Gson;
import com.servidor.entity.Registros;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class ServerThread extends Thread {
    private DataInputStream inputStream;
    private DataOutputStream outputStream;
    private Socket socket;


    public ServerThread(Socket socket) {
        this("ServerThread");
        try {
            this.socket = socket;
            inputStream = new DataInputStream(socket.getInputStream());
            outputStream = new DataOutputStream(socket.getOutputStream());
            this.start();
        } catch (IOException e) {
            System.out.println("Connection:" + e.getMessage());
        }
    }

    private ServerThread(String name) {
        super(name);
    }

    /**
     * Servidor general
     */
    public void run() {

        Gson gson = new Gson();
        //lectura de dato del cliente
        String inputData = readData();
        System.out.println("Recibido: " + inputData);
        //creacion de clase API
        Comunication comunication = gson.fromJson(inputData, Comunication.class);
        //procesamiento de datos y generacion de respuesta para el cliente
        String response = procesar(comunication);
        System.out.println("Por enviar: " + response);
        //envío de datos al cliente
        sendData(response);
        //logs
        Registros logs = new Registros(inputData, response);
        logs.save();

    }

    /**
     * Lee el dato enviado por le cliente
     *
     * @return El dato en formato String
     */
    private String readData() {
        try {
            int nb = inputStream.readInt();
            byte[] digit = new byte[nb];
            for (int i = 0; i < nb; i++)
                digit[i] = inputStream.readByte();
            return (new String(digit));
        } catch (IOException e) {
            System.out.println("IO read:" + e.getMessage());
            return (null);
        }
    }

    /**
     * Envía un String al cliente
     *
     * @param data
     */
    private void sendData(String data) {
        try {
            outputStream.writeInt(data.length());
            outputStream.writeBytes(data);

        } catch (IOException e) {
            System.out.println("IO send:" + e.getMessage());
        } finally {
            try {
                socket.close();
            } catch (IOException e) {
                System.out.println("IO: send" + e.getMessage());
            }
        }
    }

    private String procesar(Comunication comunication) {
        Registros logs;
        try {
            return (comunication.response());
        } catch (CuentaSinSaldoException e) {
            logs = new Registros(new Comunication(Error.cuentaSinSaldo).toJson());
            logs.save();
            System.out.println("cuentaSinSaldo:" + e.getMessage());
            return (new Comunication(Error.cuentaSinSaldo).toJson());
        } catch (CuentaInvalidaException e) {
            logs = new Registros(new Comunication(Error.cuentaInvalida).toJson());
            logs.save();
            System.out.println("cuentaInvalida:" + e.getMessage());
            return (new Comunication(Error.cuentaInvalida).toJson());
        } catch (MismaCuentaException e) {
            logs = new Registros(new Comunication(Error.mismaCuenta).toJson());
            logs.save();
            System.out.println("mismaCuenta:" + e.getMessage());
            return (new Comunication(Error.mismaCuenta).toJson());
        }
    }

}