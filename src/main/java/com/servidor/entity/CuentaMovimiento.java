package com.servidor.entity;


import com.google.gson.Gson;
import com.servidor.statics.TipoMovimiento;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "cuenta_movimiento")
public class CuentaMovimiento implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Cuenta cuenta;
    private Movimiento movimiento;
    private TipoMovimiento tipoMovimiento;

    public CuentaMovimiento(Cuenta cuenta, Movimiento movimiento, TipoMovimiento tipoMovimiento) {
        this.cuenta = cuenta;
        this.movimiento = movimiento;
        this.tipoMovimiento = tipoMovimiento;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id")
    public Cuenta getCuenta() {
        return cuenta;
    }

    public void setCuenta(Cuenta cuenta) {
        this.cuenta = cuenta;
    }

    public TipoMovimiento getTipoMovimiento() {
        return tipoMovimiento;
    }

    public void setTipoMovimiento(TipoMovimiento tipoMovimiento) {
        this.tipoMovimiento = tipoMovimiento;
    }

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id")
    public Movimiento getMovimiento() {
        return movimiento;
    }

    public void setMovimiento(Movimiento movimiento) {
        this.movimiento = movimiento;
    }

    public String toJson() {
        Gson gson = new Gson();
        return (gson.toJson(this));
    }
}
