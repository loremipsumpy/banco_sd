package com.servidor.entity;

import com.google.gson.Gson;
import com.servidor.statics.Estado;
import com.servidor.statics.Genero;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "usuario")
public class Usuario implements Serializable {

    @Id
    @Column(name = "ci")
    private int ci;

    private String nombre;

    private String apellido;

    private Genero genero;

    @Temporal(TemporalType.DATE)
    @Column(name = "fecha_de_nacimiento")
    private Date fechaNacimiento;

    private Estado estado;

    public Usuario() {

    }

    public Usuario(int ci, String nombre, String apellido, Genero genero, Date fechaNacimiento, Estado estado) {
        this.ci = ci;
        this.nombre = nombre.toLowerCase();
        this.apellido = apellido.toLowerCase();
        this.genero = genero;
        this.fechaNacimiento = fechaNacimiento;
        this.estado = estado;
    }

    public int getCi() {
        return ci;
    }

    public void setCi(int ci) {
        this.ci = ci;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    @Enumerated(EnumType.ORDINAL)
    public Genero getGenero() {
        return genero;
    }

    public void setGenero(Genero genero) {
        this.genero = genero;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    @Enumerated(EnumType.ORDINAL)
    public Estado getEstado() {
        return estado;
    }

    @Override
    public String toString() {
        return nombre + ' ' + apellido;
    }

    public String toJson() {
        Gson gson = new Gson();
        return (gson.toJson(this));
    }
}
