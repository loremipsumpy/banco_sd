package com.servidor.entity;

import com.google.gson.Gson;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

@Entity
@Table(name = "movimiento")
public class Movimiento implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private double monto;
    @Basic
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaMovimiento;


    public Movimiento() {

    }

    public Movimiento(double monto) {
        this.monto = monto;
        this.fechaMovimiento = Calendar.getInstance().getTime();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public double getMonto() {
        return monto;
    }

    public void setMonto(double monto) {
        this.monto = monto;
    }

    public Date getFechaMovimiento() {
        return fechaMovimiento;
    }

    public void setFechaMovimiento(Date fechaMovimiento) {
        this.fechaMovimiento = fechaMovimiento;
    }

    public String toJson() {
        Gson gson = new Gson();
        return (gson.toJson(this));
    }
}
